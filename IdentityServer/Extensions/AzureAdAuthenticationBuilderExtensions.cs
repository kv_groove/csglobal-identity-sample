﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace IdentityServer.Extensions
{
    public static class AzureAdAuthenticationBuilderExtensions
    {        
        public static AuthenticationBuilder AddAzureAd(this AuthenticationBuilder builder)
            => builder.AddAzureAd(new AzureAdConfig());

        public static AuthenticationBuilder AddAzureAd(this AuthenticationBuilder builder, AzureAdConfig azureConfig)
        {
            builder.AddOpenIdConnect("AD", options =>
            {
                options.ClientId = azureConfig.ClientId;
                options.Authority = azureConfig.Authority;   // V2 specific
                options.UseTokenLifetime = true;
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters.ValidateIssuer = false;     // accept several tenants
                options.CallbackPath = azureConfig.CallbackPath;
                options.SignedOutCallbackPath = azureConfig.SignedOutCallbackPath;
            });
            return builder;
        }
    }

    public class FilterGroupClaimsTransformation : IClaimsTransformation
    {
        private readonly ICollection<string> _allowedGroupIds;
 
        public FilterGroupClaimsTransformation(IOptions<AzureAdConfig> adConfigOptions)
        {
            _allowedGroupIds = adConfigOptions.Value.AllowedGroupIds;
        }
 
        public Task<ClaimsPrincipal> TransformAsync(ClaimsPrincipal principal)
        {
            if (!(principal.Identity is ClaimsIdentity identity))
                return Task.FromResult(principal);

            var unused = identity.FindAll(GroupsToRemove).ToList();
            unused.ForEach(c => identity.TryRemoveClaim(c));
            return Task.FromResult(principal);
        }
 
        private bool GroupsToRemove(Claim claim)
        {
            return claim.Type == "groups" &&
                   !_allowedGroupIds.Contains(claim.Value);
        }
    }
}
