﻿using System.Threading.Tasks;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;

namespace IdentityServer.Extensions
{
    public static class AzureAdB2CAuthenticationBuilderExtensions
    {
        public static AuthenticationBuilder AddAzureAdB2C(this AuthenticationBuilder builder)
            => builder.AddAzureAdB2C(new AzureAdB2CConfig());

        public static AuthenticationBuilder AddAzureAdB2C(this AuthenticationBuilder builder, AzureAdB2CConfig b2CConfig)
        {
            builder.AddOpenIdConnect("B2C", options =>
            {
                options.ClientId = b2CConfig.ClientId;
                options.Authority = b2CConfig.GetAuthority();
                options.UseTokenLifetime = true;
                options.CallbackPath = b2CConfig.CallbackPath;
                options.SignedOutCallbackPath = b2CConfig.SignedOutCallbackPath;

                options.Events = new OpenIdConnectEvents()
                {
                    OnRemoteFailure = OnRemoteFailure
                };
            })
            .AddOpenIdConnect("B2C-RSPW", options =>
            {
                options.ClientId = b2CConfig.ClientId;
                options.Authority = b2CConfig.GetAuthority(b2CConfig.ResetPasswordPolicyId);
                options.CallbackPath = b2CConfig.CallbackPath;
                options.SignedOutCallbackPath = b2CConfig.SignedOutCallbackPath;
            });
            return builder;
        }

        public static Task OnRemoteFailure(RemoteFailureContext context)
        {
            context.HandleResponse();
            // Handle the error code that Azure AD B2C throws when trying to reset a password from the login page 
            // because password reset is not supported by a "sign-up or sign-in policy"
            if (context.Failure is OpenIdConnectProtocolException && context.Failure.Message.Contains("AADB2C90118"))
            {
                // If the user clicked the reset password link, redirect to the reset password route
                context.Response.Redirect("/Account/ResetPassword");
            }
            else if (context.Failure is OpenIdConnectProtocolException && context.Failure.Message.Contains("access_denied"))
            {
                context.Response.Redirect("/");
            }
            else
            {
                context.Response.Redirect("/Home/Error?message=" + context.Failure.Message.Replace("\\", ""));
            }
            return Task.FromResult(0);
        }
    }
}
